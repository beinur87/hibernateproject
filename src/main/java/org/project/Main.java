package org.project;

import org.project.dao.AddressDao;
import org.project.dao.PersonDao;
import org.project.entities.Address;
import org.project.entities.AddressId;
import org.project.entities.BuildingType;
import org.project.entities.Person;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    private static AddressDao addressDao = new AddressDao();
    private static PersonDao personDao = new PersonDao();

    public static void main(String[] args) {
        AddressId addressId = new AddressId("Primaverii", 14, "Cluj-Napoca");
        Address address = new Address(addressId, 150, BuildingType.PRIVATE_HOUSE);
        addressDao.createAddress(address);
        System.out.println(addressDao.readAddress(addressId));

        Person ion = new Person("193040567842345", "Ion", 1993, address);
        Person vlad = new Person("177040567842345", "Vlad", 1977, address);
        Person mihai = new Person("186040567842345", "Mihai", 1986, address);
        Person dorin = new Person("168040567842345", "Dorin", 1968, address);
        Person maria = new Person("289121298983556", "Maria", 1989, address);
        Person ioana = new Person("299121298983556", "Ioana", 1999, address);
        Person simona = new Person("278121298983556", "Simona", 1978, address);


        personDao.createPerson(ion);
        personDao.createPerson(vlad);
        personDao.createPerson(mihai);
        personDao.createPerson(dorin);
        personDao.createPerson(maria);
        personDao.createPerson(ioana);
        personDao.createPerson(simona);



        personDao.readAllPersons().forEach((p) -> {
            prettyDisplayOfPerson(p);
        });

        simona.setName("Irina");
        personDao.updatePerson(simona);

        personDao.readAllPersons().forEach((p) -> {
            prettyDisplayOfPerson(p);
        });
        personDao.readAllPersons().forEach( Main::prettyDisplayOfPerson);

        Map<Integer, String> numbers = new HashMap<>();
        numbers.put(11, "unsprezece");
        numbers.put(100, "o sută");
        numbers.put(-2, "minus doi");

        numbers.forEach((k, v) -> {
            System.out.println("Numărul " + k + " se pronunță " + v);
        });



        System.out.println("All males");
        personDao.getAllMales().forEach(System.out::println);


        System.out.println("YOUNG");
        personDao.youngerThan(1990).forEach(System.out::println);

        System.out.println("Din Cluj-Napoca");
        personDao.fromCity("Cluj-Napoca").forEach(System.out::println);



    }

    static void prettyDisplayOfPerson(Person p) {
        System.out.println(p.getName()
                + " nascut in " + p.getYearOfBirth() +
                 " locuieste in "+p.getAddress().getId().getCity());
    }

    static void prettyPrint(Integer nr, String traducere) {
        System.out.println("+++ Numărul " + nr + " se pronunță " + traducere + " ++++");

    }
}
