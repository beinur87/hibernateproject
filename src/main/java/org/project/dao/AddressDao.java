package org.project.dao;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.project.database.DatabaseConfig;
import org.project.entities.Address;
import org.project.entities.AddressId;

import java.util.List;

//Data access object
//Create, Read, Update, Delete
public class AddressDao {

    public Address createAddress(Address address){
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(address);
        transaction.commit();
        session.close();
        return address;
    }

    public Address readAddress(AddressId addressId){
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Address address = session.find(Address.class,addressId);
        return address;
    }

    public List<Address> readAllAddresses(){
        Session session = DatabaseConfig.getSessionFactory().openSession();
        //cauta in entitate
        //Address este numele entitatii
        List<Address> addresses = session.createQuery("select a from Address a",Address.class).getResultList();
        session.close();
        return addresses;
    }

    public Address updateAddress(Address address){
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(address);
        transaction.commit();
        session.close();
        return address;
    }
    public Address removeAddress(Address address){
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(address);
        transaction.commit();
        session.close();
        return address;
    }

}
