package org.project.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.project.database.DatabaseConfig;
import org.project.entities.Address;
import org.project.entities.Person;

import java.util.List;

public class PersonDao {
    public Person createPerson(Person person) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(person);
        transaction.commit();
        session.close();
        return person;
    }

    public Person readPerson(String cnp) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Person person = session.find(Person.class, cnp);
        session.close();
        return person;
    }

    public List<Person> readAllPersons(){
        Session session = DatabaseConfig.getSessionFactory().openSession();
        //cauta in entitate
        //Person este numele entitatii
        List<Person> persons = session.createQuery("select a from Person a",Person.class).getResultList();
        session.close();
        return persons;
    }
    public Person updatePerson(Person person) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(person);
        transaction.commit();
        session.close();
        return person;
    }

    public Person removePerson(Person person) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.remove(person);
        transaction.commit();
        session.close();
        return person;
    }

    public List<Person> youngerThan(Integer year) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        List<Person> result = session.createQuery("select p from Person p where yearOfBirth > :yearParam", Person.class )
                .setParameter("yearParam", year)
                .getResultList();
        session.close();
        return result;
    }

    public List<Person> fromCity(String city) {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        List<Person> result = session.createQuery("select p from Person p where p.address.id.city like :cityParam", Person.class )
                .setParameter("cityParam", city)
                .getResultList();
        session.close();
        return result;
    }



    public List<Person> getAllMales() {
        Session session = DatabaseConfig.getSessionFactory().openSession();
        List<Person> males = session.createQuery("select p from Person p where p.cnp like :maleCnp ", Person.class)
                .setParameter("maleCnp", "1%")
                .getResultList();
        session.close();
        return males;
    }


}
