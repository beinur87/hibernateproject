package org.project.database;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.PersistenceStandardEntityResolver;
import org.project.entities.Address;
import org.project.entities.Person;


public class DatabaseConfig {
    private static SessionFactory sessionFactory = null;

    private DatabaseConfig(){
    }

    public static SessionFactory getSessionFactory(){
        if (sessionFactory == null){
            sessionFactory = new Configuration()
                    .configure("hibernate.config.xml")
                    .addAnnotatedClass(Address.class)
                    .addAnnotatedClass(Person.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}