package org.project.entities;

public enum BuildingType {
    RESIDENTIAL, OFFICE, PRIVATE_HOUSE, DUPLEX
}
